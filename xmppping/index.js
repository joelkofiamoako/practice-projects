var lib = require("xmpp-bosh-client/node");
// when using with Node.js
// when using with angular/react (execution in browser)

var USERNAME = "jod@meet.lindeas.com";
var PASSWORD = "";
var URL = "http://meet.lindeas.com/http-bind?room=test";

var client = new lib.BoshClient(USERNAME, PASSWORD, URL);
client.on("error", function (e) {
  console.log("Error event");
  console.log(e);
});
client.on("online", function () {
  console.log("Connected successfully");
});
client.on("ping", function () {
  console.log("Ping received at " + new Date());
});
client.on("stanza", function (stanza) {
  console.log("Stanza received at %s", new Date());
  console.log(stanza);
});
client.on("offline", function () {
  console.log("Disconnected/Offline");
});

client.connect();

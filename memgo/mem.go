package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("/proc/meminfo")
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	MemTotal := strings.TrimSpace(scanner.Text()[9:24])
	scanner.Scan()
	MemFree := strings.TrimSpace(scanner.Text()[9:24])

	MemTotalInt, err := strconv.Atoi(MemTotal)
	if err != nil {
		log.Fatal("could not convert to int")
	}

	MemFreeInt, err := strconv.Atoi(MemFree)

	if err != nil {
		log.Fatal("could not convert to int")
	}

	MemUsedInt := (MemTotalInt * 1000) - (MemFreeInt * 1000)
	MemUsed := strconv.Itoa(MemUsedInt)
	fmt.Println(MemUsed)
}

package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World")
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	ws, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		log.Error(err)
	}

	messagetype, message, err := ws.ReadMessage()
	if err != nil {
		log.Error(err)
	}
	fmt.Println(string(message))

	err = ws.WriteMessage(messagetype, message)
	if err != nil {
		log.Error(err)
	}

}

func setupRoutes() {
	http.HandleFunc("/", wsEndpoint)
}

func main() {
	fmt.Println("Listening on http://localhost:8080")
	setupRoutes()
	log.Fatal(http.ListenAndServe(":8080", nil))

}

curl --include \
     --no-buffer \
     --header "Connection: Upgrade" \
     --header "Upgrade: websocket" \
   --header "Sec-WebSocket-Version: 13" \
     http://localhost:8080/ws

const { curly } = require("node-libcurl");

const validateJitsiDomain = async (domain) => {
  let url = `https://${domain}/interface_config.js`;
  const { statusCode, data, headers } = await curly
    .get(url)
    .then(function (response) {
      console.dir(response);
      console.log(data);
      console.log(
        "Console.log => " + statusCode + " typeof " + typeof statusCode
      );
      console.log("Console.log => " + headers + " typeof " + typeof headers);
    })
    .catch(function (error) {
      console.log(error);
    });
  if (statusCode == 200) {
    valid = true;
  } else {
    valid = false;
  }
};
let validate_domain = validateJitsiDomain.call(this, "meet.lindeas.com");

console.dir(validate_domain);

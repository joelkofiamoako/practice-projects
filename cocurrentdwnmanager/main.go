package main

import (
	//"fmt"
	"fmt"
	"github.com/schollz/progressbar"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strconv"
	"sync"
	"syscall"
	"time"
)

type Download struct {
	Url           string
	TargetPath    string
	TotalSections int
}

var wg sync.WaitGroup

func main() {
	startTime := time.Now()

	// Increase resources limitations
	var rLimit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
	rLimit.Cur = rLimit.Max
	if err := syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}

	dwn := Download{

		Url:           "https://video.twimg.com/ext_tw_video/1294591688757989379/pu/vid/480x480/6DGK-96OiLYdvmyq.mp4?tag=10",
		TargetPath:    "file.mp4",
		TotalSections: 50,
	}

	err := dwn.Do()
	if err != nil {
		log.Error(err)
	}

	log.Infof("Download completed within %v	seconds", time.Now().Sub(startTime).Seconds())

}

func (dwn *Download) getNewRequest(method string) (*http.Request, error) {
	request, err := http.NewRequest(method, dwn.Url, nil)
	if err != nil {
		log.Error(err)
	}
	return request, err

}

// Do is being exported
func (dwn *Download) Do() error {

	log.Infof("Making connection...")
	request, err := dwn.getNewRequest("HEAD")
	if err != nil {
		log.Error(err)
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Error(err)
	}

	log.Infof("Got Status Code %v ", response.StatusCode)

	if response.StatusCode > 299 {

		return fmt.Errorf("Can't Process response %v", response.StatusCode)
	}

	size, err := strconv.Atoi(response.Header.Get("Content-Length"))
	log.Infof("Size of file is %v bytes", size)
	if err != nil {
		log.Error(err)
	}
	sections := dwn.divideBytes(size)
	count := len(sections) * 2
	// create and start new bar
	bar := progressbar.Default(int64(count))
	wg.Add(len(sections))

	log.Infof("Downloading File...")
	for index, section := range sections {

		index := index
		section := section
		go func() {
			defer wg.Done()
			err := dwn.downloadSection(index, section)
			if err != nil {
				log.Error(err)
			}
		}()

		bar.Add(1)
		time.Sleep(40 * time.Millisecond)
	}

	defer response.Body.Close()

	wg.Wait()

	bar.Finish()
	log.Infof("Download Complete")
	log.Infof("Merging Files...")
	dwn.mergeBytesToFile(sections)

	log.Infof("Merge Complete")
	return nil

}

func (dwn *Download) downloadSection(index int, section [2]int) error {
	request, err := dwn.getNewRequest("GET")
	if err != nil {
		log.Errorln(err)
	}
	t := &http.Transport{
		Dial: (&net.Dialer{
			Timeout:   60 * time.Second,
			KeepAlive: 30 * time.Second,
		}).Dial,
		// We use ABSURDLY large keys, and should probably not.
		TLSHandshakeTimeout: 60 * time.Second,
	}
	client := &http.Client{
		Transport: t,
	}
	request.Header.Set("Range", fmt.Sprintf("bytes=%v-%v", section[0], section[1]))
	response, err := client.Do(request)
	if err != nil {
		log.Error(err)
	}
	defer response.Body.Close()

	b, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Error(err)
	}
	err = ioutil.WriteFile(fmt.Sprintf("section-%v.tmp", index), b, 0644)
	if err != nil {
		log.Fatal(err)

	}

	//log.Infof("Downloaded %v bytes in section %v: %v", response.Header.Get("Content-Length"), index, section)
	return nil
}
func (dwn *Download) divideBytes(size int) [][2]int {

	sections := make([][2]int, dwn.TotalSections)
	eachSize := size / dwn.TotalSections
	//log.Infof("Each Section size in bytes is %v bytes", eachSize)

	//log.Println(len(sections))

	for i := range sections {
		//log.Infof("range: %v", i)

		if i == 0 {
			sections[i][0] = 0

		} else {

			sections[i][0] += sections[i-1][1] + 1
		}

		if i < dwn.TotalSections-1 {

			sections[i][1] = sections[i][0] + eachSize
		} else {
			//sections[i][1] = size - 1
			sections[i][1] = size
		}
	}

	//log.Info(sections)
	//fmt.Println(sections)
	return sections
}

func (dwn *Download) mergeBytesToFile(sections [][2]int) error {
	file, err := os.OpenFile(dwn.TargetPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	for i := range sections {

		b, err := ioutil.ReadFile(fmt.Sprintf("section-%v.tmp", i))
		if err != nil {
			log.Fatal(err)
		}

		os.Remove(fmt.Sprintf("section-%v.tmp", i))
		if err != nil {
			log.Fatal(err)
		}

		_, err = file.Write(b)
		if err != nil {
			log.Fatal(err)
		}

		//log.Infof("%v bytes merged into %v", newfilebytes, dwn.TargetPath)
	}

	return nil
}

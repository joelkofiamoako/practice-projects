package main

import (
	"log"
	"time"

	xmpp "github.com/mattn/go-xmpp"
)

func main() {
	client, err := xmpp.NewClientNoTLS("meet.appcost.me", "recoder@meet.appcost.me", "myrecorderpassword", true)

	if err != nil {
		log.Fatal(err)
	}
	client.JoinMUC("internal.auth.meet.appcost.me", "rec", 1, 1, &time.Time{})

}

package main

import (
	"fmt"
	"unsafe"
)

func main() {

	var a uint8 = 255
	var ai int8 = 127
	var b uint16 = 65535
	var bi int16 = 32767

	//Size of uint8 in bytes
	fmt.Printf("%d bytes\n", unsafe.Sizeof(a))
	fmt.Printf("%d bytes\n", unsafe.Sizeof(ai))
	fmt.Printf("%d bytes\n", unsafe.Sizeof(b))
	fmt.Printf("%d bytes\n", unsafe.Sizeof(bi))
}

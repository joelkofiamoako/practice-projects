package main

import (
	"fmt"
	"math"
)

type geometry interface {
	area() float64
	perimeter() float64
	name() string
}

type rect struct {
	width, height float64
}

type circle struct {
	radius float64
}

func (r rect) area() float64 {

	return r.width * r.height
}

func (r rect) perimeter() float64 {

	return 2*r.width + 2*r.height
}

func (r rect) name() string {

	return "rectangle"
}
func (c circle) area() float64 {

	return math.Pi * c.radius * c.radius
}

func (c circle) perimeter() float64 {

	return 2 * math.Pi * c.radius
}

func (c circle) name() string {

	return "circle"
}
func measure(g geometry) {

	fmt.Println(g.name())
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perimeter())
	fmt.Println()
}

func main() {

	r := rect{width: 3, height: 4}
	c := circle{radius: 5}

	measure(r)
	measure(c)
}
